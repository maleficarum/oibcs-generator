'use strict';
const Generator = require('yeoman-generator');
const chalk = require('chalk');
const yosay = require('yosay');

module.exports = class extends Generator {
  prompting() {
    // Have Yeoman greet the user.
    this.log(
      yosay(`Welcome to the posh ${chalk.red('generator-oibcs')} generator!`)
    );

    const prompts = [
      {
        type: 'input',
        name: 'botName',
        message: 'How you will name your bot ?',
        default: true
      }
    ];

    return this.prompt(prompts).then(props => {
      // To access props later use this.props.someAnswer;
      this.props = props;
    });
  }

  writing() {
    this.log("Creating component registry ...");
    this.fs.copy(
      this.templatePath('registry.js'),
      this.destinationPath(this.props.botName + '/registry.js')
    );

    this.log("Creating component skeleton ...");
    this.fs.copy(
      this.templatePath('mycomponent'),
      this.destinationPath(this.props.botName + '/mycomponent')
    );    
  }

  install() {
    this.installDependencies();
  }
};
