"use strict"

const self = module.exports = {

    metadata: () => ({
        "name": "MyComponent",
        "properties": {
        },
        "supportedActions": [
        ]
    }),

    invoke: (conversation, done) => {
        conversation.logger().info('Properties ' + conversation.properties());

        conversation.reply({ text: "Hi there"});
      
        conversation.transition();
        done();                   
    }
};